#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"
#if 0
#include<iostream>
using namespace std;
struct A {

	long a1;

	short a2;

	int a3;

	int* a4;

};

int main()
{

	cout << sizeof(struct  A) << endl;//64位编译器大小是24，32位编译器大小是16

	return 0;
}


#endif

#if 1


int main()
{
	Date d1(2020, 2, 2);
	Date d2(2024, 4, 4);
	/*d1.Print();
	d2.Print();*/

	cout << d1 << d2 << endl;
	cin >> d1 >> d2;
	cout << d1 << d2 << endl;


	/*int tmp1 = d1 - d2;
	int tmp2 = d2 - d1;
	cout << tmp1 << " " << tmp2 << endl;*/



	////关系运算符
	//int a = d1 > d2;
	//int b = d1 == d2;
	//int c = d1 >= d2;
	//int d = d1 < d2;
	//int e = d1 <= d2;
	//int f = d1 != d2;

	//cout << a << " " << b << " " << c << " " << d << " " << e << " " << f << endl;





	////前置后置++测试
	//Date tmp1 = d1++;
	//tmp1.Print();

	//++d1;
	//d1.Print();

	////前置后置--测试
	//Date tmp2 = d1--;
	//tmp2.Print();

	//--d1;
	//d1.Print();

	/*d1 -= 13;
	d1.Print();
	Date tmp = d1 - 20;
	tmp.Print();*/

	/*Date d2(2021, 2, 2);
	Date d3(2020, 3, 3);
	Date d4(d3);
	int tmp1 = d1.GetMonthDay(2020, 2);
	int tmp2 = d2.GetMonthDay(2021, 2);
	int tmp3 = d3.GetMonthDay(2020, 3);
	cout << tmp1 <<" "<< tmp2 <<" "<< tmp3 << endl;*/
	
	

	return 0;
}

#endif