#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"

//检查日期合法性
bool Date::CheckDate()const
{
	if (_day > GetMonthDay(this->_year, this->_month) || _month > 12)
		return false;
	return true;
}

// 获取某年某月的天数
int Date:: GetMonthDay(int year, int month)const
{
	int getdays[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	if ((month == 2) && ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)))
	{
		return getdays[month] + 1;//闰年二月天数+1
	}
	return getdays[month];
}

// 全缺省的构造函数
Date::Date(int year, int month , int day )
{
	_year = year;
	_month = month;
	_day = day;

	if (!(this->CheckDate()))
	{
		cout << "日期非法" << endl << *this << endl;
	}
}

//拷贝构造函数
// d2(d1)
Date:: Date(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
 }

// 赋值运算符重载
// d2 = d3 -> d2.operator=(&d2, d3)
 Date& Date::operator=(const Date& d)
{
	 _year = d._year;
	 _month = d._month;
	 _day = d._day;
	 return *this;//保证运算符可以连续赋值
}

 // 析构函数
 Date::  ~Date()
 {
	 //无需释放资源
}

 // 日期+=天数
 Date& Date::operator+=(int day)
 {
	 _day += day;
	 int tmp = GetMonthDay(this->_year, this->_month);
	 while (_day > tmp)//日期大于本月天数
	 {
		 _day -= tmp;//就减去本月天数
		 ++_month;//月份+1
		 if (_month > 12)//月份大于12
		 {
			 ++_year;//年份+1，月份置为1
			 _month = 1;
		 }
		 tmp = GetMonthDay(this->_year, this->_month);
	 }
	 return *this;
 }

 // 日期+天数
 Date Date::operator+(int day)const
 {
	 Date tmp = *this;//复用+=代码
	 tmp += day;
	 return tmp;
 }

 // 日期-天数
 Date Date::operator-(int day)const
 {
	 Date tmp = *this;
	 tmp -= day;
	 return tmp;
 }

 // 日期-=天数
 Date& Date::operator-=(int day)
 {
	 _day -= day;
	 while (_day < 1)
	 {
		 --_month;//向前借位
		 if (_month == 0)
		 {
			 --_year;
			 _month = 12;
		 }
		 _day+= GetMonthDay(this->_year, this->_month);

	 }
	 return *this;
 }

 // 前置++
 Date& Date::operator++()
 {
	 *this += 1;
	 return *this;
 }

 // 后置++
 Date Date::operator++(int)
 {
	 Date tmp = *this;
	 *this += 1;
	 return tmp;
 }

 // 后置--
 Date Date::operator--(int)
 {
	 Date tmp = *this;
	 *this -= 1;
	 return tmp;
 }

 // 前置--
 Date& Date::operator--()
 {
	 *this -= 1;
	 return *this;
 }

 // >运算符重载
 bool Date::operator>(const Date& d)const
 {
	 if (_year > d._year)//先比较年
		 return true;
	 else if (_year == d._year)//年相等比较月
	 {
		 if (_month > d._month)
			 return true;
		 else if (_month == d._month)//月相等比较天
		 {
			 if (_day > d._day)
				 return true;
		 }
	 }
	 return false;//上述条件都没有执行，就返回false
 }

 // ==运算符重载
 bool Date::operator==(const Date& d)const
 {
	 return _year == d._year && _month == d._month && _day == d._day;
 }

 // >=运算符重载
 bool Date::operator >= (const Date& d)const
 {
	 return *this > d || *this == d;//复用大于和等于
 }

 // <运算符重载
 bool Date::operator < (const Date& d)const
 {
	 return !(*this > d) && !(*this == d);//复用大于和等于

 }

 // <=运算符重载
 bool Date::operator <= (const Date& d)const
 {
	 return !(*this > d);
 }

 // !=运算符重载
 bool Date::operator != (const Date& d)const
 {
	 return !(*this == d);
 }

 // 日期-日期 返回天数
 int Date::operator-(const Date& d)const
 {
	 int flag = 1;
	 int count = 0;
	 Date max = *this;//假设this大，d小
	 Date min = d;

	 if (max < min)//假设不对，则更改
	 {
		 max = d;
		 min = *this;
		 flag = -1;
	 }
	 while (min != max)//使用计数器计算
	 {
		 ++min;
		 ++count;
	 }
	 return flag * count;
 }





 //运算符重载
 ostream& operator<<(ostream& out,const Date& d)
 {
	 out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
	 return out;//方便连续使用
 }

 //运算符重载
 istream& operator>>(istream& in, Date& d) 
 {
	 cout << "请输入年 月 日" << endl;
	 while (1)
	 {
		 in >> d._year >> d._month >> d._day;
		 if (d.CheckDate())
			 break;
		 cout << "输入的日期非法，请重新输入！！！" << endl;
	 }
	 return in;
 }

