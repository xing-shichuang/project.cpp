﻿#define _CRT_SECURE_NO_WARNINGS 1

//#include<iostream>
//using namespace std;


//函数重载
#if 0
// 1、参数类型不同

int Add(int left, int right)
{
	cout << "int Add(int left, int right)" << endl;
	return left + right;
}
double Add(double left, double right)
{
	cout << "double Add(double left, double right)" << endl;
	return left + right;
}
// 2、参数个数不同

void f()
{
	cout << "f()" << endl;
}
void f(int a)
{
	cout << "f(int a)" << endl;
}
// 3、参数类型顺序不同

void f(int a, char b)
{
	cout << "f(int a,char b)" << endl;
}
void f(char b, int a)
{
	cout << "f(char b, int a)" << endl;
}

// 返回值不同不能作为重载条件，因为调用时无法区分

//void fxx()
//{}
//
//int fxx()
//{
// return 0;
//}


// 下⾯两个函数构成重载

// f1()调用时，会报错，存在歧义，编译器不知道调用谁

void f1()
{
	cout << "f()" << endl;
}

void f1(int a = 10)
{
	cout << "f(int a)" << endl;
}

int main()
{
	Add(10, 20);
	Add(10.1, 20.2);
	f();
	f(10);
	f(10, 'a');
	f('a', 10);
	return 0;
}

#endif


//引用
#if 0

//类型标识符& 引用名 = 已存在的变量名;

#include<iostream>
using namespace std;

void swap(int& x, int& y) {
	int temp = x;
	x = y;
	y = temp;
}
int main()
{
	int a = 0;
	// 引⽤：b和c是a的别名

	int& b = a;
	int& c = a;
	// 也可以给别名b取别名，d相当于还是a的别名

	int& d = b;
	++d;
	// 这⾥取地址我们看到是⼀样的



	cout << &a << endl;
	cout << &b << endl;
	cout << &c << endl;
	cout << &d << endl;
	return 0;
}
int main1()
{
	const int a = 10;
	// 编译报错：error C2440: “初始化”: ⽆法从“const int”转换为“int &”
	// 这⾥的引⽤是对a访问权限的放⼤

	//int& ra = a;
	// 这样才可以

	const int& ra = a;
	// 编译报错：error C3892: “ra”: 不能给常量赋值

	//ra++;
	// 这⾥的引⽤是对b访问权限的缩⼩

	int b = 20;
	const int& rb = b;
	// 编译报错：error C3892: “rb”: 不能给常量赋值

	//rb++;
	return 0;
}

#endif


#if 1

#include<iostream>
using namespace std;
// 计算⼀下A/B/C实例化的对象是多⼤？

class A
{
public:
	void Print()
	{
		cout << _ch << endl;
	}
private:
	char _ch;
	int _i;
};
class B
{
public:
	void Print()
	{
		//...
	}
};
class C
{};
int main()
{
	A a;
	B b;
	C c;
	cout << sizeof(a) << endl;
	cout << sizeof(b) << endl;
	cout << sizeof(c) << endl;
	return 0;
}

#endif